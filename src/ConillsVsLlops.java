//versió 0
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConillsVsLlops {
    public static final int CONILLS = 7, LLOPS = 5, COLUMNES = 16, FILES = 16, ROQUES = 15, AIGUA = 15;
    public static ArrayList<Animal> animals = new ArrayList<>();
    private static ArrayList<Animal> llops = new ArrayList<>();
    private static ArrayList<Animal> conills = new ArrayList<>();
    private static ArrayList<Animal> conillsMorts = new ArrayList<>();
    private static ArrayList<Animal> llopsMorts = new ArrayList<>();
    public static Casella[][] tauler = new Casella[COLUMNES][FILES];
    public static Casella[][] buit = new Casella[COLUMNES][FILES];
    private int torns;
    private static int totalConills;
    private static int totalLlops = 0;

    public static void main(String[] args){

        ConillsVsLlops joc = new ConillsVsLlops();
        joc.inicialitza();
        Scanner scanner = new Scanner(System.in);
        while ((conillsMorts.size() != totalConills) && (llopsMorts.size() != totalLlops)){

            for (Animal animal : animals){
                animal.mou();
            }
            for (Animal animal : animals){
                if(animal instanceof Conill && animal.mort) conillsMorts.add(animal);
                else if (animal instanceof Llop && animal.mort) llopsMorts.add(animal);
            }
            for (Animal animal : conillsMorts){
                animals.remove(animal);
                conills.remove(animal);
            }
            for (Animal animal : llopsMorts){
                animals.remove(animal);
                llops.remove(animal);
            }
            joc.torns ++;
            joc.mostra();
            String accio = scanner.nextLine();
            if (accio.equals("bubitos")) {
                conillsMorts = conills;
                llopsMorts = llops;
                for (Animal animal: animals) {
                    animal.mor();
                }
                joc.mostra(accio);
                System.out.println("My girlfriend is the best person in the world <3");
                System.out.println("El poder de l'amor t'ha ajudat a guanyar!");
                System.out.println();
                break;
            }

        }
        System.out.println("Has destruit part de la vida a la vista, bon treball \uD83D\uDE05 !");
    }

    /**
     * Inicialitza el tauler amb els blocs i animals corresponents.
     */
    public void inicialitza(){
        for (int i = 0; i < COLUMNES ; i++) {
            for (int j = 0; j < FILES; j++) {
                int terreny = (int) (Math.random() * 100), t = 0;
                int animal = (int) (Math.random() * 100);
                if (terreny < ROQUES) t = 1;
                else if (terreny < ROQUES + AIGUA) t = 2;
                Animal a = null;
                if (t == 2 || animal >= LLOPS + CONILLS) {
                    tauler[i][j] = new Casella(t);
                }
                else{
                    if (animal < LLOPS) {
                        a = new Llop(i, j);
                        animals.add(a);
                    } else if (animal < LLOPS + CONILLS) {
                        a = new Conill(i, j);
                        animals.add(a);
                    }
                    tauler[i][j] = new Casella(t, a);
                }
                buit[i][j] = new Casella(t);
            }
        }
        for (Animal animal: animals) {
            if (animal instanceof Conill){
                Conill c = new Conill(animal.x, animal.y);
                conills.add(c);
            }
            else if (animal instanceof Llop){
                Llop l = new Llop(animal.x, animal.y);
                llops.add(l);
            }
        }
        totalConills = conills.size();
        totalLlops = llops.size();
    }

    /**
     * Mostra el tauler amb les files i columnes pertinents.
     */
    public void mostra(){
        for (int i = 0; i < FILES; i++) {
            for (int j = 0; j < COLUMNES; j++) {
                System.out.print(tauler[i][j]);
            }
            System.out.println();
        }
        System.out.println("\uD83E\uDDE1 Conills: " + (conills.size() - conillsMorts.size()) + "/" + totalConills + "  \uD83D\uDD50" + torns);
        System.out.println("\uD83E\uDDE1 Llops: " + (llops.size() - llopsMorts.size()) + "/" + totalLlops);
    }
    /**
     * Mostra el tauler amb les files i columnes pertinents EASTER EGG.
     */
    public void mostra(String bubitos){
        for (int i = 0; i < FILES; i++) {
            for (int j = 0; j < COLUMNES; j++) {
                System.out.print(buit[i][j]);
            }
            System.out.println();
        }
        System.out.println("\uD83E\uDDE1 Conills: " + (conills.size() - conillsMorts.size()) + "/" + totalConills + "  \uD83D\uDD50" + torns);
        System.out.println("\uD83E\uDDE1 Llops: " + (llops.size() - llopsMorts.size()) + "/" + totalLlops);
    }
}

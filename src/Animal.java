abstract class Animal{
    int x, y;
    boolean mort = false;
    public void mor() {
        this.mort = true;
    }
    public abstract String toString();

    /**
     * S'encarrega de moure cad un de les peces de la classe Animal, en cada un dels torns.
     */
    public void mou(){}


    /**
     * Té la posició del Animal
     * @param x Mostra la coordenada x
     * @param y Mostra la coordenada y
     */
    public Animal(int x, int y){
        this.x = x;
        this.y = y;
    }
}
class Casella {
    private int terreny = 0; // 0(terra, verd) 1(roca, gris) 2(aigua, blau)
    private Animal animal = null;

    /**
     * Mostra el Casella corresponent
     * @return Retorna el Casella amb el seu fons, i en cas que hi hagui un Animal el mostrarà també.
     */
    public String toString(){
        String grafic;
        switch (terreny){
            case 0: grafic = "\u001B[42m"; break;
            case 1: grafic = "\033[0;100m"; break;
            case 2: grafic = "\u001B[44m"; break;
            default: grafic = "\u001B[42m"; // terra per defecte
        }
        if(animal == null) return grafic + "  " + "\033[0m"; // 2 espais per a mantindre l'alineació

        return grafic + animal + "\033[0m"; // reset, torna al color per defecte
    }

    /**
     * Inicialitza un Casella pasan-li un Integer de terreny per parametre.
     * @param terreny
     */
    public Casella(int terreny){
        this.terreny = terreny;
    }


    /**
     * Inicialitza un Casella pasan-li un Integer de terreny per parametre, i un animal.
     * @param terreny
     * @param animal
     */
    public Casella(int terreny, Animal animal){
        this.animal = animal;
        this.terreny = terreny; // this(terreny); no era així per a reutilitzar un constructor sobrecarregat? :S
    }

    /**
     * Determina si el Casella és aigua.
     * @return True si el resultat és aigua, False si no ho és.
     */
    public boolean esHerba() {
        if(terreny == 0) return true;
        return false;
    }

    /**
     * Determina si el Casella és aigua.
     * @return True si el resultat és aigua, False si no ho és.
     */
    public boolean esAigua() {
        if(terreny == 2) return true;
        return false;
    }

    /**
     * Determina si el Casella és roca.
     * @return True si el resultat és roca, False si no ho és.
     */
    public boolean esRoca() {
        if(terreny == 1) return true;
        return false;
    }

    /**
     * Determina si el Casella conté un animal.
     * @return True si conté un animal, False si no el conté
     */
    public boolean esAnimal(){
        if(animal != null) return true;
        return false;
    }

    /**
     * Determina si el Casella conté un Conill.
     * @return True si conté un Conill, False si no el conté
     */
    public boolean esConill(){
        if(animal instanceof Conill) return true;
        return false;
    }

    /**
     * Determina si el Casella conté un Llop.
     * @return True si conté un Llop, False si no el conté
     */
    public boolean esLlop(){
        if(animal instanceof Llop) return true;
        return false;
    }

    /**
     * @return Retorna l'animal dins el Casella.
     */
    public Animal getAnimal() { return this.animal; }

    public void delAnimal(){
        this.animal = null;
    }
    public void setAnimal(Animal animal){
        this.animal = animal;
    }
}
class Llop extends Animal{
    int energia = 10;

    /**
     * Retorna String que representa el LLop
     * @return String representatiu del llop
     */
    @Override

    public String toString() { return "\uD83D\uDC3A"; } // icona de llop

    /**
     * Mou el llop a la seguent casella aleatoria dins de 1 moviment.
     */
    @Override
    public void mou(){
        Casella[][] tauler = ConillsVsLlops.tauler;
        if(energia == 0){
            mor();                      // eliminem les referències a aquest objecte i confiem en el
            tauler[x][y].delAnimal();   // garbage collector per a destruir-lo (ja que no té referències)
        }
        else {
            int i = (int) (Math.random() * 3) - 1 + x;
            int j = (int) (Math.random() * 3) - 1 + y;
            boolean dinsTauler = i >= 0 && i < tauler.length && j >= 0 && j < tauler[0].length;
            if (dinsTauler && !tauler[i][j].esAigua() && !tauler[i][j].esLlop() && !tauler[i][j].esRoca() && !(tauler[i][j].esConill() && tauler[i][j].esRoca())) {
                if (tauler[i][j].esConill()) {
                    tauler[i][j].getAnimal().mor();
                    energia += 10;
                }
                tauler[x][y].delAnimal();
                tauler[i][j].setAnimal(this);
                x = i;
                y = j;
                energia --;
            }
        }
    }

    /**
     * Reutilitzem el constructor de la classe pare.
     * @param x Fa referencia a la coordenada x
     * @param y Fa referencia a la coordenada y
     */
    public Llop(int x, int y){ super(x, y); }
}
class Conill extends Animal{
    int vida = 2;
    /**
     * Determina el dibuix que té un conill
     * @return Retorna un String que representarà un conill.
     */
    @Override
    public String toString() {
        return "\uD83D\uDC30";
    }

    /**
     * Mou el Conill a la seguent casella aleatoria dins de 1 moviment.
     * Si és queda sense vida mor
     */
    @Override
    public void mou(){
        Casella[][] tauler = ConillsVsLlops.tauler;
        if(vida == 0){
            mor();                      // eliminem les referències a aquest objecte i confiem en el
            tauler[x][y].delAnimal();   // garbage collector per a destruir-lo (ja que no té referències)
        }
        else {
            int i = (int) (Math.random() * 3) - 1 + x;
            int j = (int) (Math.random() * 3) - 1 + y;
            boolean dinsTauler = i >= 0 && i < tauler.length && j >= 0 && j < tauler[0].length;
            if (dinsTauler && !tauler[i][j].esAigua() && !tauler[i][j].esLlop() && !tauler[i][j].esConill()) {
                if (tauler[i][j].esHerba()) {
                    vida = 2;
                }
                else{
                    vida--;
                }
                tauler[x][y].delAnimal();
                tauler[i][j].setAnimal(this);
                x = i;
                y = j;
            }
        }
    }

    /**
     * Reutilitzem el constructor de la classe pare.
     * @param x Fa referencia a la coordenada x
     * @param y Fa referencia a la coordenada y
     */
    public Conill(int x, int y) {
        super(x, y);
    }
}